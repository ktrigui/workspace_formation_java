package com.btk;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Author Ktrigui
 */
public class App 
{
    public static void main( String[] args ){
    	try {
        // Chargement de connecteur (driver)
    	Class.forName("com.mysql.jdbc.Driver");
    	
    	
    	// Connexion sur la BD
    	
    	String url = "jdbc:mysql://localhost:3306/BanqueDB";
    	String user = "root";
    	String password = "password";
    	
    	Connection connection = 
    			DriverManager.getConnection(url, user, password);
    	
    	
    	// Formulationn dle requete
    	 Statement statement = connection.createStatement();
    	 
    	// Excécution de la requete 
    	
    	 ResultSet resultSet = 
    			 statement.executeQuery("select * from comptes");
    	 
    	 // Exploitation des résultats
    	 while(resultSet.next()) { 
    		 String num = resultSet.getString("Numero");
    		 String proprio = resultSet.getString("Proprietaire");
    		 BigDecimal solde = resultSet.getBigDecimal("Solde");
    		 
    		 System.out.println("Compte : "+ num + " - "+proprio+ "-"+solde);
    	 }
    	 
    	 
    	 
    	 // fermeture
    	 connection.close();
    	 
    } catch (ClassNotFoundException | SQLException ex) {
    	// TODO Auto-generated catch block
    	ex.printStackTrace();
    }
    }
}
