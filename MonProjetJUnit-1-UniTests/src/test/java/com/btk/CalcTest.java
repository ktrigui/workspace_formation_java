package com.btk;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.btk.business.Calcul;

public class CalcTest {
	private Calcul calcul;

	@Before
	public void setUp() throws Exception {
		calcul = new Calcul();
	}

	@After
	public void tearDown() throws Exception {
		calcul = null;
	}

	@Test
	public void testAdditionner() {

		// ARRANGE

		int a = 11;
		int b = 22;
		int expected = 33;
		// ACT
		int actual = calcul.additionner(a, b);

		// ASSERT
		assertEquals(expected, actual);
	}

}
