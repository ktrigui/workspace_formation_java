package com.btk;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.btk.business.Compte;

public class CompteTest {

	private Compte underTest;

	@Before
	public void setUp() throws Exception {
		underTest = new Compte("C1000", "SAADAOUI Nizar", new BigDecimal("1000"));
	}

	@After
	public void tearDown() throws Exception {
		underTest = null;
	}

	@Test
	public void testCréditer() {
		// Arrange
		BigDecimal somme = new BigDecimal("450");
		BigDecimal expected = new BigDecimal("1450");
		// Act
		underTest.créditer(somme);
		BigDecimal actual = underTest.getSolde();
		// assert
		assertEquals(expected, actual);
	}

	@Test
	public final void testDébiter() {
		// Arrange
		BigDecimal somme = new BigDecimal("450");
		BigDecimal expected = new BigDecimal("1450");
		// Act
		underTest.créditer(somme);
		BigDecimal actual = underTest.getSolde();
		// assert
		assertEquals(expected, actual);
	}

}
